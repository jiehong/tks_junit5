package com.gitlab.jiehong;

import org.junit.jupiter.api.Test;

import java.util.function.Predicate;

import static org.assertj.core.api.Assertions.assertThat;

class SecondLeapYearCheckerTest {
  private final Predicate<Integer> sut = new LeapYearChecker();

  @Test void test0ne() {
    assertThat(sut.test(2000)).isTrue();
  }
}
