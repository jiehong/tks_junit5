package com.gitlab.jiehong;

import org.junit.jupiter.api.Test;

import java.util.function.Predicate;

import static org.assertj.core.api.Assertions.assertThat;

class FirstLeapYearCheckerTest {
  // sut = System Under Test
  // It's a generic, short name instead of `leapYearChecker` or `leapYearService` or `xxxClient`, etc.
  private final Predicate<Integer> sut = new LeapYearChecker();

  @Test void testOne() {
    assertThat(sut.test(2000)).isTrue();
  }
}
