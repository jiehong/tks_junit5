package com.gitlab.jiehong;

import java.util.function.Predicate;

public class LeapYearChecker implements Predicate<Integer> {
  @Override
  public boolean test(final Integer year) {
    return year % 400 == 0 || (year % 4 == 0 && year % 100 != 0);
  }
}
