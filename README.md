# Java Junit 5 review

## Write some tests

A LeapYearChecker implementation is given, and you need to test it.

Write tests in FirstLeapYearCheckerTest.java until you're satisfied.

## Unit-tests

“A failing test should tell you _exactly_ what is wrong _quickly_,
without having to spend a lot of time analysing the failure.” — Marit van Dijk

“Use meaningful descriptive names, telling us _why_ [the test] does this.” – Marit van Dijk
